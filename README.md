
h1. prerequites
- vagrant-guest_ansible
  - https://github.com/vovimayhem/vagrant-guest_ansible

h1. Usage
- vagrant destroy server -f; vagrant destroy agent -f
- vagrant plugin install vagrant-guest_ansible; vagrant up

h1. Architecture and Keep knowing.
- vagrant server: scouter server
- vagrant agent : scouter host, tomcat, stress, jmeter(in the future)
- scouter client: you should install client program in your desktop(or laptop)
  Just so you know, if you gonna use scouter.client in the windows, you should install oracle jdk 1.8.0.
  when I use the openjdk 11, it failed.